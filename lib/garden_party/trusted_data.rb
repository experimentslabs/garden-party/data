# frozen_string_literal: true

require 'active_support/core_ext/string/inflections'
require 'active_support/log_subscriber'

require_relative 'trusted_data/format_validator'
require_relative 'trusted_data/pack'

require_relative 'trusted_data/collection_errors'

require_relative 'trusted_data/collection/families'
require_relative 'trusted_data/collection/genera'
require_relative 'trusted_data/collection/resources'

require_relative 'trusted_data/record/family'
require_relative 'trusted_data/record/genus'
require_relative 'trusted_data/record/resource'

require_relative 'trusted_data/version'

ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'genus', 'genera'
end

module GardenParty
  module TrustedData
    DATA_PATH = File.join(File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'data')))
    class Error < StandardError; end
  end
end
