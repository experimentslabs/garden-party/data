# frozen_string_literal: true

require 'yaml'
require 'json'

require 'garden_party/trusted_data/collection/families'
require 'garden_party/trusted_data/collection/genera'
require 'garden_party/trusted_data/collection/resources'
require 'garden_party/trusted_data/collection/resource_interactions_groups'
require 'garden_party/trusted_data/collection/resource_interactions'

module GardenParty
  module TrustedData
    # Manipulate packs
    class Pack # rubocop:disable Metrics/ClassLength
      attr_accessor :path
      attr_reader :collection_names

      # Initializes the class with either a file to load or a pack hash
      #
      # @param name [Symbol] Pack name, overrides `file` when specified
      # @param pack_hash [Hash] Pack hash
      # @param file [String] Path to the YAML pack file
      def initialize(name = nil, pack_hash: nil, file: nil)
        raise 'Use "pack_hash" or "file", not both' if file && pack_hash

        file = File.join(DATA_PATH, "#{name}.yml") if name

        load file if file

        initialize_collections pack_hash if pack_hash
      end

      # Loads a pack file
      #
      # @param file [String] Path to the YAML pack file
      def load(file)
        @path = file
        initialize_collections YAML.load_file file
      end

      # Saves the pack to file
      #
      # @param file [String, nil] Path to the pack file. Uses the same paths as `load` if defined.
      def save(file = nil)
        file ||= @path
        raise 'No output file specified' if file.blank?

        hash = save_collections(to_h)

        File.write file, hash.to_yaml
      end

      # Validate every collection and check for crossed references
      def validate # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
        # rubocop:disable Layout/LineLength
        @collection_names.each { |name| @collections[name].validate }

        @collections[:families].check_usage :name, @collections[:genera], :family
        @collections[:genera].check_usage :name, @collections[:resources], :genus

        @collections[:genera].check_existence :family, @collections[:families], :name
        @collections[:resources].check_existence :genus, @collections[:genera], :name
        @collections[:resources].check_existence :parent, @collections[:resources], :name
        @collections[:resources].check_existence :interaction_group, @collections[:resource_interactions_groups], :name

        @collections[:resource_interactions].check_existence :subject, @collections[:resource_interactions_groups], :name
        @collections[:resource_interactions].check_existence :target, @collections[:resource_interactions_groups], :name

        @collections[:resource_interactions_groups].check_usage :name, @collections[:resources], :interactions_group
        @collections[:resource_interactions].check_usage :subject, @collections[:resource_interactions_groups], :name
        @collections[:resource_interactions].check_usage :target, @collections[:resource_interactions_groups], :name

        # rubocop:enable Layout/LineLength
        @errors = nil

        valid?
      end

      def normalize
        @collection_names.each { |name| @collections[name].normalize }

        # Return nil to prevent console output of the full lists
        nil
      end

      # Synchronizes data with another pack.
      # Rules:
      #   - Add every new and/or modified data
      #   - Do not remove data if absent from new pack
      #   - Merge arrays (tags, sources, etc...)
      #
      # This is still a work in progress
      def synchronize(pack)
        @collection_names.each do |name|
          @collections[name].synchronize pack.send(name)
        end
      end

      def strip_geodata!
        @collection_names.each { |name| @collections[name].strip_geodata! }
      end

      def valid?
        @collection_names.each { |name| return false unless @collections[name].valid? }

        true
      end

      def errors
        @errors ||= {}
        @collection_names.each do |name|
          @errors[name] = @collections[name].errors
        end

        @errors
      end

      def display_errors
        errors.each_pair do |collection, errors|
          puts "- #{collection}"
          errors.display
        end

        nil
      end

      # Complete missing IDs in records
      def add_ids
        @collection_names.each { |name| @collections[name].add_ids }

        # Return nil to prevent console output of the full lists
        nil
      end

      def to_h
        hash = {}
        @collection_names.each { |name| hash[name] = @collections[name].to_h }

        hash
      end

      def to_yaml
        to_h.to_yaml
      end

      class << self
        def clone(name, new_name, keep_geodata: false)
          pack = new name
          pack.path = File.join(DATA_PATH, "#{new_name}.yml")
          pack.strip_geodata! unless keep_geodata

          pack
        end
      end

      private

      # @param [Hash] hash
      def initialize_collections(hash)
        # rubocop:disable Layout/HashAlignment, Layout/LineLength
        @collections = {
          # @type [Collection::Families]
          families: Collection::Families.new(hash[:families] || []),
          # @type [Collection::Genera]
          genera: Collection::Genera.new(hash[:genera] || []),
          # @type [Collection::Resources]
          resources: Collection::Resources.new(hash[:resources] || []),
          # @type [Collection::ResourceInteractionsGroups]
          resource_interactions_groups: Collection::ResourceInteractionsGroups.new(hash[:resource_interactions_groups] || []),
          # @type [Collection::ResourceInteractions]
          resource_interactions: Collection::ResourceInteractions.new(hash[:resource_interactions] || []),
        }
        # rubocop:enable Layout/HashAlignment, Layout/LineLength

        @collection_names = @collections.keys
      end

      def method_missing(method, *args, &block)
        return @collections[method] if @collection_names.include? method

        super method, args, block
      end

      def respond_to_missing?(method, include_private = false)
        @collection_names.include?(method) || super
      end

      # Save sub-collections in the pack hash
      # @param export_hash [Hash] Hash resulting of `to_h`
      def save_collections(export_hash)
        require 'byebug'
        @collection_names.each do |collection_name|
          collection_path = @collections[collection_name].path
          if collection_path
            File.write(File.join(DATA_PATH, "#{collection_path}.yml"), { data: send(collection_name).to_h }.to_yaml)
            export_hash[collection_name] = collection_path
          end
        end

        export_hash
      end
    end
  end
end
