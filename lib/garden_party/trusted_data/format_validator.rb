# frozen_string_literal: true

module GardenParty
  module TrustedData
    class ValidationError < StandardError; end

    # Utility class to validate values
    class FormatValidator
      class << self
        def validate!(value, format)
          return _validate_enum(value, format) if format.is_a? Array

          validator = "validate_#{format}".to_sym
          raise "Unhandled format #{format}" unless private_methods.include? validator

          send(validator, value)
        end

        private

        # Special validator to use when format is an array
        #
        # @param list  [Array] Allowed values
        # @param value [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def _validate_enum(value, list)
          raise ValidationError, "is not an allowed value (#{list.join(', ')})" unless list.include? value
        end

        # General types

        # Validates that value is an array
        #
        # @param value  [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_array(value)
          raise ValidationError, 'is not an array' unless value.is_a? Array
        end

        # Validates that value is either true or false
        #
        # @param value  [*] Value to test

        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_boolean(value)
          raise ValidationError, 'is not a boolean' unless value.is_a?(TrueClass) || value.is_a?(FalseClass)
        end

        # Validates that value is a hash
        #
        # @param value  [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_hash(value)
          raise ValidationError, 'is not a hash' unless value.is_a? Hash
        end

        # Validates that value is an integer
        #
        # @param value  [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_integer(value)
          raise ValidationError, 'is not an integer' unless value.is_a? Integer
        end

        # Validates that value is either an integer or a float
        #
        # @param value  [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_number(value)
          raise ValidationError, 'is not a number' unless value.is_a?(Integer) || value.is_a?(Float)
        end

        # Validates that value is a string
        #
        # @param value  [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_string(value)
          raise ValidationError, 'is not a string' unless value.is_a? String
        end

        # Arrays

        # Validates that value is an array with unique values
        #
        # @param values [*] Value to test
        # @param validate_array  [TrueClass, FalseClass] Whether or not to check array type too
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_integers_list(values, validate_array: true)
          self.validate_array values if validate_array
          values.each { |v| raise ValidationError, 'must contain only integers' unless v.is_a? Integer }
        end

        # Validates that value is an array of strings
        #
        # @param values [*] Value to test
        # @param validate_array  [TrueClass, FalseClass] Whether or not to check array type too
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_strings_list(values, validate_array: true)
          self.validate_array values if validate_array
          values.each { |v| raise ValidationError, 'must contain only strings' unless v.is_a? String }
        end

        # Validates that value is an array with unique values
        #
        # @param values  [*] Value to test
        # @param validate_array  [TrueClass, FalseClass] Whether or not to check array type too
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_uniques_list(values, validate_array: true)
          self.validate_array values if validate_array
          raise ValidationError, 'must have unique values' unless values.count == values.uniq.count
        end

        # Hashes

        # Validates that value is a hash of symbol keys and string values, with some content.
        #
        # @param value [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_symbol_string_hash(value, validate_hash: true)
          self.validate_hash value if validate_hash
          value.each_pair do |k, v|
            raise ValidationError, 'has non-symbol values' unless k.is_a? Symbol
            raise ValidationError, 'has non-string values' unless v.is_a?(String) && v.length.positive?
          end
        end

        # Specific types

        # Validates that value is a list of unique integers, from 1 to 12
        #
        # @param values [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_months_list(values)
          validate_integers_list values
          validate_uniques_list values, validate_array: false
          values.each { |v| raise ValidationError, 'must contain only integers' unless v.is_a? Integer }
          raise ValidationError, 'must have values between 1 and 12 included' unless values.max <= 12 && values.min >= 1
        end

        # Validates that value is an array with unique tags
        #
        # @param values [*] Value to test
        #
        # @return [void]
        # @raise [ValidationError] when value is invalid
        def validate_tags_list(values)
          validate_strings_list values
          validate_uniques_list values, validate_array: false

          values.each { |v| raise ValidationError, 'separators are "::"' if /(:::|\w:\w)/.match?(v) }
        end
      end
    end
  end
end
