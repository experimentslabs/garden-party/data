# frozen_string_literal: true

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/family'

module GardenParty
  module TrustedData
    module Collection
      class Families < Base
      end
    end
  end
end
