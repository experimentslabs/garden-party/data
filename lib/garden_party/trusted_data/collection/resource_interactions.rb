# frozen_string_literal: true

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/resource_interaction'

module GardenParty
  module TrustedData
    module Collection
      # Represents a resource interaction list
      class ResourceInteractions < Base
      end
    end
  end
end
