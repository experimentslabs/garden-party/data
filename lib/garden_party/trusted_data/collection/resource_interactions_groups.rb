# frozen_string_literal: true

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/resource_interactions_group'

module GardenParty
  module TrustedData
    module Collection
      # Represents a resource interactions group list
      class ResourceInteractionsGroups < Base
      end
    end
  end
end
