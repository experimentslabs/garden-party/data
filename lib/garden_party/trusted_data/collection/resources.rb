# frozen_string_literal: true

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/resource'

module GardenParty
  module TrustedData
    module Collection
      # Represents a resources list
      class Resources < Base
        # Default validation + validation of unique external_ids
        def validate
          super

          @list.each do |record|
            validate_external_ids record
            validate_ancestry record
          end
        end

        private

        def validate_external_ids(record)
          return unless record.values[:external_ids].is_a? Array

          record.values[:external_ids].each_pair do |key, id|
            count = 0

            @list.each do |r|
              count += 1 if r.values.dig(:external_ids, key) == id
              break if count > 1
            end

            @errors.add_error record, "Duplicate external id: #{key} / #{id}" if count > 1
          end
        end

        def validate_ancestry(record)
          return unless record.values[:parent]

          parent = find name: record.values[:parent]
          @errors.add_error record, "Parent not found: #{record.values[:parent]}" unless parent
          return unless parent && parent.values[:parent]

          @errors.add_error record, "Parent has parent: #{record.values[:parent]}; #{parent.values[:parent]}"
        end
      end
    end
  end
end
