# frozen_string_literal: true

require 'active_support/core_ext/enumerable'
require 'active_support/core_ext/module/delegation'

require 'garden_party/trusted_data/collection_errors'

module GardenParty
  module TrustedData
    module Collection
      # Represents a records collection
      #
      # This is the base class and should only by inherited
      class Base # rubocop:disable Metrics/ClassLength
        attr_reader :list, :errors, :path

        # @param list_or_path [String, [Hash, Record::Base]] String for a relative path, list of hashes, records or both
        def initialize(list_or_path)
          if list_or_path.is_a? String
            load_path list_or_path
          else
            load_list list_or_path
          end
        end

        # Adds a record to the list
        # @param data [Hash, GardenParty::TrustedData::Record::Base]
        def add(data, set_id: true)
          record = case data
                   when self.class.record_class
                     data
                   when Hash
                     self.class.record_class.new data
                   else
                     raise 'What kind of data is that?'
                   end
          record.set :id, next_id if set_id && !record.values[:id]

          @list.push record
        end

        # Find records matching all pairs values
        #
        # @param pairs [Hash]
        #
        # @return [Array<Record>]
        #
        # Example:
        #   find_all color: 'red'
        #   => [{id: 10, name: 'Butternut', color: 'red', tags: ['edible']},
        #       {id: 12, name: 'Red flower', color: 'red' }]
        #   find_all( color: 'red') { |record| record.get(:tags).include? 'edible' }
        #   => [{id: 10, name: 'Butternut', color: 'red', tags: ['edible']}]
        def find_all(pairs = {}, &block)
          results = _find pairs, first: false

          results.select!(&block) if block

          results
        end

        # Find records where values contains all pairs values
        #
        # @param pairs [Hash]
        #
        # @return [Array<Record>]
        #
        # Example:
        #   find_all_partial name: 'utter'
        #   => [{id: 10, name: 'Butternut'},
        #       {id: 12, name: 'Red butternut'},
        #       {id: 120, name: 'Blue butternut'}]
        #   find_all_partial( name: 'utter') { |record| record.get(:id) < 100 }
        #   => [{id: 10, name: 'Butternut'}, {id: 12, name: 'Red butternut'}]
        def find_all_partial(pairs = {}, &block)
          expected_matches = pairs.keys.count
          @list.find_all do |record|
            matches = 0
            pairs.each_pair { |name, value| matches += 1 if record.get(name)&.include? value }

            matches == expected_matches && (block ? yield(record) : true)
          end
        end

        # Find first record where values contains all pairs values
        #
        # @param pairs [Hash]
        #
        # @return [Record]
        #
        # Example:
        #   find id: 10
        #   => { id: 10, name: 'Butternut' }
        def find(pairs)
          _find pairs, first: true
        end

        # Updates all records in list with new attributes
        #
        # When no list is provided, the whole collection is updated.
        # An additional block can be passed to modify the record.
        #
        # @param list [Array<Record>, nil] Array of records
        # @param attributes [Hash] Hash of new attribute/value pairs
        #
        # Example:
        #    update_all attributes: {color: 'red'}
        #    update_all find_all(color: 'red'), attributes: {color: 'blue'}
        #    update_all find_all(color: 'red') { |record| record.set(:color, 'red') }
        def update_all(list = nil, attributes = {}, &block)
          list ||= @list
          list.each do |record|
            attributes.each_pair { |attribute, value| record.set(attribute, value) }

            record = yield(record) if block
          end
        end

        def synchronize(collection)
          collection.list.each do |record|
            current_record = find id: record.get(:id)
            if current_record && record.get(:id)
              current_record.synchronize record
            else
              puts "New record: #{record}"
              # add the record "as is"
              @list.push(record)
            end
          end
        end

        def strip_geodata!
          @list.each(&:strip_geodata!)
        end

        # Checks if unique fields are really unique amongst the collection
        #
        # @param record [[Hash, Record::Base]] Record to check
        def check_uniqueness(record)
          self.class.unique_fields.each do |field|
            amount = @list.count { |r| r.values[field].to_s.casecmp(record.values[field].to_s).zero? }
            @errors.add_error record, "'#{field}' is used for #{amount} records" if amount > 1
          end
        end

        # Checks if a record is referenced in target collection
        #
        # ex: @users.check_usage :name, @comments, :user
        #
        # @param record_field [Symbol] Record field that may have the referenced value
        # @param collection [GardenParty::TrustedData::Collection::Base] Collection from which to process the values
        # @param collection_field [Symbol] Field that must have the reference value in the collection
        def check_usage(record_field, collection, collection_field)
          values = collection.pluck collection_field

          @list.each do |record|
            next if values.include? record.values[record_field]

            message = "'#{record_field}' is not used as reference in #{collection.name} '#{collection_field}' field"
            @errors.add_warning record, message
          end
        end

        # ex: @comments.check_existence :user, @users, :name
        #
        # @param record_field [Symbol] Reference field
        # @param collection [GardenParty::TrustedData::Collection::Base] Collection in which to search the value
        # @param collection_field [Symbol] Field that must have the value in the collection
        def check_existence(record_field, collection, collection_field)
          values = collection.pluck collection_field

          @list.each do |record|
            next if record.values[record_field].blank?

            record_value = record.values[record_field]
            next if values.include? record_value

            @errors.add_error record,
                              "#{collection.name} does not have an entry with #{collection_field} '#{record_value}'"
          end
        end

        def last_id
          pluck(:id).compact.max || 0
        end

        def next_id
          last_id + 1
        end

        # Adds missing ids in records
        def add_ids
          # Find the next ID
          id = next_id

          @list.map! do |record|
            next record if record.values[:id].present?

            record.set :id, id
            id += 1

            record
          end
        end

        # Validates each record, checks unique fields amongst the collection
        def validate
          @errors.reset
          @list.each do |record|
            record.validate
            check_uniqueness record
            @errors.add_field_errors record
          end
        end

        # Normalize every record and removes empty ones
        def normalize
          add_ids
          @list.reject!(&:blank?)
          @list.each(&:normalize)
          @list.sort! { |a, b| a.values[:id] <=> b.values[:id] }
        end

        def valid?
          !@errors.errors?
        end

        # Human readable string to represent the collection
        def name
          self.class.name.demodulize.underscore.humanize
        end

        # Method to sort the records.
        #
        # Has to be implemented on children classes
        def sort!
          raise NotImplementedError
        end

        # Wrapper to search in the list
        def _find(pairs, first: true)
          expected_matches = pairs.keys.count

          @list.send(first ? :find : :find_all) do |record|
            matches = 0
            pairs.each_pair { |name, value| matches += 1 if record.get(name) == value }

            matches == expected_matches
          end
        end

        # Delegation
        def to_h
          @list.map(&:to_h)
        end

        # Delegation
        delegate :count, to: :list

        # Delegation
        def pluck(attrs)
          @list.map(&:values).pluck attrs
        end

        class << self
          # Associated record class; inferred from the class name
          #
          # @return [Record::Base] Record class
          def record_class
            @record_class ||= "GardenParty::TrustedData::Record::#{name.to_s.demodulize.singularize}".constantize
          end

          # Field definition from record class
          #
          # @return [Hash] Field definitions
          def record_fields
            @record_fields ||= record_class.fields
          end

          # Fields that should be unique
          #
          # @return [[Symbol]] Field list
          def unique_fields
            @unique_fields ||= record_fields.keys.select { |k| record_fields[k][:unique] == true }
          end
        end

        private

        # Loads a YAML file and create collection from its `:data` key
        # @param path [String] String for a relative path from data directory
        def load_path(path)
          file = File.join(DATA_PATH, "#{path}.yml")
          raise "Missing file #{file}" unless File.exist? file

          collection = YAML.load_file file
          @path = path

          load_list collection[:data]
        end

        # Loads a list of hashes or Record as the collection content
        # @param list [[Hash, Record::Base]] List of hashes, records or both
        def load_list(list)
          @errors = CollectionErrors.new
          # @type [[Record::Base]]
          @list = []

          list.each { |record| add record, set_id: false }
        end
      end
    end
  end
end
