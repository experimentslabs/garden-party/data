# frozen_string_literal: true

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/genus'

module GardenParty
  module TrustedData
    module Collection
      class Genera < Base
      end
    end
  end
end
