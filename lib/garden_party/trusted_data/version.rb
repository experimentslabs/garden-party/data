# frozen_string_literal: true

module GardenParty
  module TrustedData
    VERSION = '0.1.0'
  end
end
