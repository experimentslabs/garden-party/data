# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      # Represents a resource
      class ResourceInteraction < Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:subject] || '??'}->#{values[:target] || '??'}"
        end

        def self.fields
          # rubocop:disable Layout/LineLength
          @fields ||= {
            id:      { required: true, format: :integer, unique: true },
            subject: { required: true, format: :string },
            target:  { required: true, format: :string },
            nature:  { required: true, format: %w[competition mutualism neutralism parasitism amensalism commensalism unspecified_positive unspecified_negative] },
            notes:   { required: false, format: :string },
            sources: { required: false, format: :strings_list, default: [] },
          }
          # rubocop:enable Layout/LineLength
        end

        def normalize
          super

          [:sources].each { |key| @values[key].sort! if @values.key? key }
        end
      end
    end
  end
end
