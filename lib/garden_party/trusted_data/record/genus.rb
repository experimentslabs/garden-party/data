# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      # Represents a genus
      class Genus < Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:name] || '??'}"
        end

        def self.fields
          @fields ||= {
            id:           { required: true, format: :integer, unique: true },
            name:         { required: true, format: :string, unique: true },
            family:       { required: true, format: :string },
            source:       { required: false, format: :string },
            external_ids: { required: false, format: :symbol_string_hash, default: {} },
          }
        end
      end
    end
  end
end
