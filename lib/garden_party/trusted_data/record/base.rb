# frozen_string_literal: true

module GardenParty
  module TrustedData
    module Record
      # Base class to represent a pack record.
      #
      # It provides validation and normalization methods
      class Base
        attr_reader :values, :errors

        # @param data [Hash] Values
        def initialize(data)
          @values = {}

          data.each_pair do |key, value|
            next unless fields.key? key

            @values[key] = value.is_a?(String) ? value.strip : value
          end

          reset_errors
        end

        def strip_geodata!
          fields.each_key do |field|
            next unless fields[field][:geodata] == true

            @values[field] = fields[field][:default]
          end
        end

        # Validates and fills errors
        #
        # Record is considered valid as long as it has not been validated once.
        def validate
          reset_errors

          fields.each_key do |field|
            validate_field field
          end
        end

        # Record is considered valid as long as it has not been validated once.
        def valid?
          validate
          @errors.each_value { |errors| return false if errors.count.positive? }

          true
        end

        # Sets a value for a given key.
        #
        # Shortcut to .values[key] = value
        def set(key, value)
          raise "'#{key}' is not allowed on this record type" unless fields.key? key

          @values[key] = value
        end

        # Gets a value for a given key
        def get(key)
          raise "'#{key}' does not exist on this record type" unless fields.key? key

          @values[key] || fields[key][:default]
        end

        # Sorts keys to have unified output in generated files, and removes empty
        # fields.
        #
        # Empty required fields are kept with their default type
        def normalize
          sort_keys!
          hash = {}

          fields.each_pair do |field, definition|
            hash[field] = @values[field] || definition[:default] if @values[field].present? || definition[:required]
            hash[field].strip! if hash[field].is_a? String
          end

          @values = hash
        end

        def synchronize(record) # rubocop:disable Metrics/MethodLength
          fields.each_pair do |field, definition|
            value = record.get(field)
            next unless value.present?

            if definition[:default].is_a? Array
              @values[field] ||= definition[:default]
              @values[field] += value
              @values[field].uniq!
            else
              @values[field] = value
            end
          end
        end

        # Delegation on values
        def to_h
          @values.to_h
        end

        # A string representing the record, mainly used as keys for error lists.
        #
        # It has to be defined on every child class
        def to_s
          raise NotImplementedError
        end

        # Returns true when all attributes are empty
        def blank?
          @values.each_value { |value| return false unless value.blank? }

          true
        end

        private

        # Sorts keys from field order definition
        def sort_keys!
          new_hash = {}
          fields.each_key { |key| new_hash[key] = @values[key] if @values.key? key }

          @values = new_hash
        end

        # Delegation to class method
        def fields
          self.class.fields
        end

        def reset_errors
          @errors = {}
          fields.each_key { |k| @errors[k] = [] }
        end

        def validate_field(field)
          validate_presence field

          # Ignore non-required fields when empty
          return unless fields[field][:required] || @values[field].present?

          validate_format field
        end

        def validate_presence(field)
          @errors[field].push 'is missing' if fields[field][:required] != false && @values[field].blank?
        end

        def validate_format(field)
          format = fields[field][:format]
          FormatValidator.validate! @values[field], format if format
        rescue ValidationError => e
          @errors[field].push e.to_s
        end

        class << self
          # Returns the field definition.
          #
          # Has to be overrode in child classes
          def fields
            raise NotImplementedError
          end
        end
      end
    end
  end
end
