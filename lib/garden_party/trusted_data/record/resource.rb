# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      # Represents a resource
      class Resource < Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:name] || '??'}"
        end

        def self.fields # rubocop:disable Metrics/MethodLength
          @fields ||= {
            id:                      { required: true,  format: :integer, unique: true },
            name:                    { required: true,  format: :string, unique: true },
            latin_name:              { required: false, format: :string },
            genus:                   { required: true,  format: :string },
            parent:                  { required: false, format: :string },
            interactions_group:      { required: false, format: :string },
            common_names:            { required: false, format: :strings_list, default: [] },
            description:             { required: false, format: :string },
            diameter:                { required: false, format: :number },
            sheltered_sowing_months: { required: false, format: :months_list, default: [], geodata: true },
            soil_sowing_months:      { required: false, format: :months_list, default: [], geodata: true },
            harvesting_months:       { required: false, format: :months_list, default: [], geodata: true },
            tags:                    { required: false, format: :tags_list, default: [] },
            sources:                 { required: false, format: :strings_list, default: [] },
            external_ids:            { required: false, format: :symbol_string_hash, default: {} },
          }
        end

        def normalize
          super

          [:sources, :tags, :common_names].each { |key| @values[key].sort! if @values.key? key }
        end
      end
    end
  end
end
