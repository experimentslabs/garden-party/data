# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      # Represents a resource
      class ResourceInteractionsGroup < Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:name] || '??'}"
        end

        def self.fields
          @fields ||= {
            id:          { required: true, format: :integer, unique: true },
            name:        { required: true, format: :string, unique: true },
            description: { required: false, format: :string },
          }
        end
      end
    end
  end
end
