# frozen_string_literal: true

module GardenParty
  module TrustedData
    # Stores errors for a collection.
    #
    # It keeps errors from records; errors and warnings concerning the collection.
    class CollectionErrors
      attr_reader :messages

      def initialize
        reset
      end

      # @param record [GardenParty::TrustedData::Record::Base]
      def add_field_errors(record)
        return if record.valid?

        initialize_for record
        @messages[record.to_s][:fields_errors] = record.errors
      end

      # @param record [GardenParty::TrustedData::Record::Base]
      # @param error [String]
      def add_error(record, error)
        add_message record, error, key: :errors
      end

      # @param record [GardenParty::TrustedData::Record::Base]
      # @param error [String]
      def add_warning(record, error)
        add_message record, error, key: :warnings
      end

      def reset
        @messages = {}
      end

      def errors?
        @messages.each_value do |record|
          return true if record[:errors].count.positive?

          record[:fields_errors].each_value do |field_errors|
            return true if field_errors.count.positive?
          end
        end

        false
      end

      def display
        @messages.each_pair do |entry, lists|
          next if lists[:warnings].empty? && lists[:errors].empty? && lists[:fields_errors].empty?

          puts "  - #{entry}:"
          display_collection lists[:warnings], :warning
          display_collection lists[:errors], :error
          display_fields lists[:fields_errors]
        end
      end

      private

      # @param record [GardenParty::TrustedData::Record::Base]
      # @param error [String]
      # @param key [:errors, :warnings]
      def add_message(record, error, key: :errors)
        initialize_for record
        @messages[record.to_s][key].push error
      end

      def initialize_for(record)
        @messages[record.to_s] ||= { errors: [], warnings: [], fields_errors: {} }
      end

      def display_collection(list, type)
        list.each do |error|
          puts ActiveSupport::LogSubscriber.new.send :color, "    #{error}", type == :error ? :red : :yellow
        end
      end

      def display_fields(record)
        record.each_pair do |field, errors|
          next if errors.blank?

          puts "    - #{field}:"
          errors.each do |error|
            puts ActiveSupport::LogSubscriber.new.send(:color, "      #{error}", :red)
          end
        end
      end
    end
  end
end
