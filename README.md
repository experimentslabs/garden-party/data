# Garden Party - trusted data

Helper methods to use
in [garden-party](https://gitlab.com/experimentslabs/garden-party/garden-party).

The gem repository also provides data files carefully completed by the community
to use as a trusted source in Garden Party instances.

The gem is not published on RubyGem as there is no need for it right now. If you
need it for another project, we may reconsider (be aware that the data _won't_
be distributed in the gem itself).

Feel free to [join the Matrix chatroom](https://app.element.io/#/room/#gp-data:matrix.org)
dedicated to this project.

## About GardenParty

Garden Party is an opensource application to help gardening newbies to manage
their garden.

Check the [Garden Party website](https://garden-party.io) for links to other
parts of the project and community links.

### Integration note

This project is in its early stage of development, and one
of the way it is intended to work in the future follow these two scenarios:

- Instance configuration:
  - the data source is declared in the instance configuration files
  - refreshes can be triggered from the administration area.
- Raw import
  - the data source is not declared in the configuration files
  - refreshes can be manually made by importing packs from the administration area.

The two use case above means that if you're an instance owner, you can fork this
repo and create your own source of truth on your side. If you want ;)

## License

- The code is under the [MIT](LICENCE) license.
- Packs data is under the [CC-0](data/LICENCE) license.

## Usage

### For GardenParty

As a Garden-party instance maintainer, you need to define the
`garden_party.trusted_datasource_url` in the
`config/garden_party_instance.yml` file (since `v0.8.0`). It
should point to the raw version of one of the data files:
`https://gitlab.com/experimentslabs/garden-party/data/-/raw/main/data/<file>.yml`.

Once set, restart the server, log-in as an administrator and head to `Import` in
the main menu.

## Geographical data

A database of this kind can be really tedious to maintain as soon as there is
localization data in it (e.g.: _sowing periods_, _harvesting periods_, ...)

If you want to submit data, check if it can fit in one of the existing packs
(in `data/`) or create a new pack. Try to follow this naming convention:
`<language>_<country>_<region>.yml`.

**Don't submit data that does not belong to the pack geographical position.**

The english pack (`data/en.yml`) is totally generic for now (no geographical
data in it). It's a good starting point for new ones.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then,
run `rake spec` to run the tests. You can also run `bin/console` for an
interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To
release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and the created tag, and push the `.gem` file
to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab
at <https://gitlab.com/experimentslabs/garden-party/data/-/issues>. This project
is intended to be a safe, welcoming space for collaboration, and contributors
are expected to adhere to the [code of conduct](CODE_OF_CONDUCT.md).

Whether you are a code contributor or a data contributor, you agree that your
contributions will fall under the two licenses used here.

You can use `bin/console` to open an IRB console with helpers to manipulate packs,
if you don't want to edit files by hand.

## Helpers

### Tasks

```sh
rake data:check      # Checks for data consistency and output issues
rake data:add_ids    # Adds missing ids based on the last ID used
rake data:normalize  # Cleans empty non-required fields
```

## Data format

_Packs_ files are the `.yml` one in `data` directory. They contain families, genera
and resources.

To be able to sync an instance with new data, a unique identifier is defined for
every family, genus and resource.

On Garden Party instances, this ID is then saved in the database to ease the
synchronisation process (`sync_id` attribute).

The formats for the different record types are described
[in the code](lib/garden_party/trusted_data/record/).

## Code of Conduct

Everyone interacting in the _GardenParty - trusted data_ project's codebase,
issue trackers, chat rooms and mailing lists is expected to follow
the [code of conduct](CODE_OF_CONDUCT.md).
