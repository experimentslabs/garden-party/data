# frozen_string_literal: true

require_relative '../records/example'

require 'garden_party/trusted_data/collection/base'

module GardenParty
  module TrustedData
    module Collection
      class Examples < Base
      end
    end
  end
end
