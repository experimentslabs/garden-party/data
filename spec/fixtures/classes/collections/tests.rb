# frozen_string_literal: true

require_relative '../records/test'

require 'garden_party/trusted_data/collection/base'

module GardenParty
  module TrustedData
    module Collection
      class Tests < Base
      end
    end
  end
end
