# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      class Example < GardenParty::TrustedData::Record::Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:test] || '??'}"
        end

        def self.fields
          @fields ||= {
            id:   { required: true, unique: true },
            # Used to test references
            test: { required: false },
          }
        end
      end
    end
  end
end
