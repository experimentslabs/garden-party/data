# frozen_string_literal: true

require 'garden_party/trusted_data/record/base'

module GardenParty
  module TrustedData
    module Record
      class Test < GardenParty::TrustedData::Record::Base
        def to_s
          "#{@values[:id] || '??'} - #{values[:name] || '??'}"
        end

        def self.fields
          @fields ||= {
            id:       { required: true, format: :integer, unique: true },
            name:     { required: true, format: :string, unique: true },
            option:   { required: false, format: :array, default: [] },
            req_list: { required: true, format: :array, default: [] },
            opt_list: { required: false, format: :array, default: [] },
          }
        end
      end
    end
  end
end
