# frozen_string_literal: true

module Helpers
  def gem_path
    File.expand_path File.join(File.dirname(__FILE__), '..', '..')
  end

  def spec_dir
    File.join(gem_path, 'spec')
  end

  def temp_dir
    File.join(gem_path, 'tmp')
  end

  def file_fixture(path)
    file_path = File.join(spec_dir, 'fixtures', 'files', path)
    raise "Fixture file does not exist: '#{file_path}'" unless File.exist? file_path

    file_path
  end
end
