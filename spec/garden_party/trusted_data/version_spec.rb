# frozen_string_literal: true

RSpec.describe GardenParty::TrustedData::VERSION do
  it 'has a version number' do
    expect(GardenParty::TrustedData::VERSION).not_to be_nil # rubocop:disable RSpec/DescribedClass
  end
end
