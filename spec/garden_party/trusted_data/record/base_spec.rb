# frozen_string_literal: true

require 'spec_helper'
require 'fixtures/classes/records/test'

require 'garden_party/trusted_data/record/base'

RSpec.describe GardenParty::TrustedData::Record::Base do
  describe '.new' do
    let(:instance) { GardenParty::TrustedData::Record::Test.new name: 'test', extra: 'something' }

    it 'filters values' do
      expect(instance.values).not_to have_key :extra
    end
  end

  describe '.validate' do
    let(:instance) { GardenParty::TrustedData::Record::Test.new({ opt_list: 'string' }) }

    before { instance.validate }

    it 'adds missing required fields to errors' do
      [:id, :name].each { |k| expect(instance.errors[k]).to include 'is missing' }
    end

    it 'adds non-array data to errors' do
      expect(instance.errors[:opt_list]).to eq ['is not an array']
    end
  end

  describe '.valid?' do
    context 'with valid data' do
      let(:instance) { GardenParty::TrustedData::Record::Test.new id: 1, name: 'something', req_list: ['value'] }

      it 'returns true' do
        expect(instance).to be_valid
      end
    end

    context 'with invalid data' do
      let(:instance) { GardenParty::TrustedData::Record::Test.new({}) }

      it 'returns true' do
        expect(instance).not_to be_valid
      end
    end
  end

  describe '.set' do
    let(:instance) { GardenParty::TrustedData::Record::Test.new({}) }

    context 'with allowed key' do
      before do
        instance.set :name, 'something'
      end

      it 'sets the value' do
        expect(instance.values[:name]).to eq 'something'
      end
    end

    context 'with non-allowed key' do
      it 'throws an error' do
        expect do
          instance.set :not_allowed_field, 'something'
        end.to raise_error "'not_allowed_field' is not allowed on this record type"
      end
    end
  end

  describe '.normalize' do
    let(:hash) do
      instance = GardenParty::TrustedData::Record::Test.new({ id: 1, name: 'the name', option: nil })
      instance.normalize
      instance.to_h
    end

    it 'contains defined fields' do
      [:id, :name, :req_list].each { |k| expect(hash).to have_key k }
    end

    it 'does not contain empty, optional fields' do
      [:option, :opt_list].each { |k| expect(hash).not_to have_key k }
    end

    it 'fills missing required fields with their default value' do
      expect(hash[:req_list]).to eq []
    end
  end
end
