# frozen_string_literal: true

require 'spec_helper'

require 'garden_party/trusted_data/collection/base'
require 'garden_party/trusted_data/record/base'

require 'fixtures/classes/collections/tests'
require 'fixtures/classes/collections/examples'

RSpec.describe GardenParty::TrustedData::Collection::Base do
  describe '.new' do
    let(:instance) { GardenParty::TrustedData::Collection::Tests.new [{ id: 1, name: 'something' }] }

    it 'converts hashes to the correct record' do
      expect(instance.list.first).to be_a GardenParty::TrustedData::Record::Test
    end
  end

  describe '.check_uniqueness' do
    let(:instance) do
      GardenParty::TrustedData::Collection::Tests.new [{ id: 1, name: 'entry' }, { id: 1, name: 'other_entry' }]
    end

    before do
      instance.validate
    end

    it 'adds errors when unique fields are duplicated' do # rubocop:disable RSpec/ExampleLength
      expected = {
        '1 - entry'       => { errors: ["'id' is used for 2 records"], warnings: [], fields_errors: {
          id: [], name: [], opt_list: [], option: [], req_list: ['is missing', 'is not an array']
        } },
        '1 - other_entry' => { errors: ["'id' is used for 2 records"], warnings: [], fields_errors: {
          id: [], name: [], opt_list: [], option: [], req_list: ['is missing', 'is not an array']
        } },
      }

      expect(instance.errors.messages).to eq expected
    end
  end

  describe '.check_usage' do
    let(:test_records) do
      GardenParty::TrustedData::Collection::Tests.new [{ id: 1, name: 'entry' }, { id: 2, name: 'unused' }]
    end
    let(:example_records) do
      GardenParty::TrustedData::Collection::Examples.new [{ id: 1, test: 'entry' }, { id: 2, test: 'missing' }]
    end

    before do
      test_records
      example_records
      test_records.check_usage :name, example_records, :test
    end

    it 'adds warning when a referenced entry is unused' do
      expected = { '2 - unused' => { errors: [],
warnings: ["'name' is not used as reference in Examples 'test' field"], fields_errors: {} } }
      actual = test_records.errors.messages

      expect(actual).to eq expected
    end
  end

  describe '.add' do
    let(:collection) do
      GardenParty::TrustedData::Collection::Tests.new [{ id: 1, name: 'entry' }, { id: 2, name: 'another' }]
    end

    it 'adds hashes' do
      hash = { id: 3, name: 'new record' }
      collection.add(hash)

      expect(collection.list.last.to_h).to eq hash
    end

    it 'adds instance of records' do
      hash = { id: 3, name: 'new record' }
      record = GardenParty::TrustedData::Record::Test.new hash
      collection.add(record)

      expect(collection.list.last.to_h).to eq hash
    end

    it 'sets id' do
      hash = { name: 'new record' }
      collection.add(hash)

      expect(collection.list.last.get(:id)).to eq 3
    end
  end

  describe '.find' do
    let(:collection) do
      GardenParty::TrustedData::Collection::Tests.new [
        { id: 1, name: 'entry' },
        { id: 2, name: 'unused' },
        { id: 3, name: 'other entry' },
      ]
    end

    it('returns the first match') do
      expect(collection.find(id: 2).to_h).to eq({ id: 2, name: 'unused' })
    end
  end

  describe '.find_all' do
    let(:collection) do
      GardenParty::TrustedData::Collection::Tests.new [
        { id: 1, name: 'entry' },
        { id: 2, name: 'unused', option: ['value'] },
        { id: 3, name: 'entry', option: ['value'] },
      ]
    end

    it('returns all the matches') do
      expected = [{ id: 1, name: 'entry' }, { id: 3, name: 'entry', option: ['value'] }]

      expect(collection.find_all(name: 'entry').map(&:to_h)).to eq expected
    end

    it('handles additional block') do
      result   = collection.find_all(name: 'entry') { |r| r.get(:option).include? 'value' }
      expected = [{ id: 3, name: 'entry', option: ['value'] }]

      expect(result.map(&:to_h)).to eq expected
    end
  end

  describe '.find_all_partial' do
    let(:collection) do
      GardenParty::TrustedData::Collection::Tests.new [
        { id: 1, name: 'entry' },
        { id: 2, name: 'unused', option: ['value'] },
        { id: 3, name: 'other entry', option: ['value'] },
      ]
    end

    it('returns all the matches') do
      expected = [{ id: 1, name: 'entry' }, { id: 3, name: 'other entry', option: ['value'] }]

      expect(collection.find_all_partial(name: 'entry').map(&:to_h)).to eq expected
    end

    it('handles additional block') do
      result   = collection.find_all_partial(name: 'entry') { |r| r.get(:option).include? 'value' }
      expected = [{ id: 3, name: 'other entry', option: ['value'] }]

      expect(result.map(&:to_h)).to eq expected
    end
  end

  describe '.update_all' do
    let(:collection) do
      GardenParty::TrustedData::Collection::Tests.new [
        { id: 1, name: 'entry' },
        { id: 2, name: 'unused', option: ['value'] },
        { id: 3, name: 'other entry', option: ['value'] },
      ]
    end

    it 'updates the list' do # rubocop:disable RSpec/ExampleLength
      list = collection.find_all_partial name: 'entry'
      collection.update_all list, option: ['hello']
      expected = [{ id: 1, name: 'entry', option: ['hello'] },
                  { id: 2, name: 'unused', option: ['value'] },
                  { id: 3, name: 'other entry', option: ['hello'] }]

      expect(collection.to_h).to eq expected
    end

    it 'handles additional block' do # rubocop:disable RSpec/ExampleLength
      list = collection.find_all_partial name: 'entry'
      collection.update_all list do |r|
        option = r.get(:option)
        option.push('another')
        r.set(:option, option)
      end
      expected = [{ id: 1, name: 'entry', option: ['another'] },
                  { id: 2, name: 'unused', option: ['value'] },
                  { id: 3, name: 'other entry', option: %w[value another] }]

      expect(collection.to_h).to eq expected
    end
  end

  describe '.check_existence' do
    let(:test_records) do
      GardenParty::TrustedData::Collection::Tests.new [{ id: 1, name: 'entry' }, { id: 2, name: 'unused' }]
    end
    let(:example_records) do
      GardenParty::TrustedData::Collection::Examples.new [{ id: 1, test: 'entry' }, { id: 2, test: 'missing' }]
    end

    before do
      test_records
      example_records
      example_records.check_existence :test, test_records, :name
    end

    it 'adds error when a referenced entry is unused' do
      expected = { '2 - missing' => { errors: ["Tests does not have an entry with name 'missing'"], warnings: [],
fields_errors: {} } }
      actual = example_records.errors.messages

      expect(actual).to eq expected
    end
  end

  describe '.add_ids' do
    let(:instance) do
      GardenParty::TrustedData::Collection::Tests.new [
        { id: 2 },
        { id: 5 },
        { id: 1 },
        { name: 'missing_id' },
      ]
    end

    before { instance.add_ids }

    it 'adds missing ids' do
      expect(instance.list[-1].values[:id]).to eq 6
    end
  end

  describe '.errors' do
    let(:instance) { GardenParty::TrustedData::Collection::Tests.new [{ id: 1 }] }

    before do
      instance.validate
    end

    it 'collects errors' do
      expected = { '1 - ??' => { errors: [], warnings: [], fields_errors: {
        id: [], name: ['is missing',
                       'is not a string'], opt_list: [], option: [], req_list: ['is missing', 'is not an array']
      } } }
      expect(instance.errors.messages).to eq expected
    end
  end
end
