# frozen_string_literal: true

require 'spec_helper'

RSpec.describe GardenParty::TrustedData::Pack do
  describe '.new' do
    context 'with a hash' do
      it 'sets the collections' do
        pack = described_class.new pack_hash: {
          families: [{ id: 1, name: 'Family One' }],
        }

        expect(pack.families.list.first.values[:name]).to eq 'Family One'
      end
    end

    context 'with a file' do
      it 'loads the file' do
        pack = described_class.new file: file_fixture('valid.yml').to_s

        expect(pack.families.list.first.values[:name]).to eq 'Some family'
      end
    end

    context 'with both "file" and "pack_hash" parameters' do
      it 'fails' do
        expect do
          described_class.new file: 'something.yml', pack_hash: { key: 'value' }
        end.to raise_error 'Use "pack_hash" or "file", not both'
      end
    end
  end

  describe '.save' do
    let(:other_file_path) { File.join(temp_dir, 'test__fixture_custom_pack.yml') }
    let(:file_path) { File.join(temp_dir, 'test__fixture_seeds.yml') }

    context 'when a file was previously loaded' do
      let(:pack) { described_class.new file: file_path }

      before do
        FileUtils.cp file_fixture('empty.yml'), file_path
        pack.families.add({ name: 'new_family' })
      end

      it 'uses the file path' do
        pack.save
        new_file_content = YAML.load_file(file_path)
        expect(new_file_content[:families].first).to have_key :name
      end

      context 'when a file path is provided' do
        before do
          FileUtils.rm other_file_path if File.exist? other_file_path
        end

        it 'uses the provided path' do
          pack.save other_file_path
          new_file_content = YAML.load_file(other_file_path)
          expect(new_file_content[:families].first).to have_key :name
        end
      end
    end

    context 'when no file was previously loaded' do
      let(:pack) do
        described_class.new pack_hash: {
          families: [{ name: 'new_value' }],
        }
      end

      before do
        FileUtils.rm other_file_path if File.exist? other_file_path
      end

      it 'fails' do
        expect do
          pack.save
        end.to raise_error 'No output file specified'
      end

      context 'when a file path is provided' do
        it 'uses the provided path' do
          pack.save other_file_path
          new_file_content = YAML.load_file(other_file_path)
          expect(new_file_content[:families].first[:name]).to eq 'new_value'
        end
      end
    end
  end

  describe '.validate, .valid?' do
    context 'when the pack is invalid' do
      let(:pack) { described_class.new file: file_fixture('invalid.yml') }

      it 'triggers validation on collections' do
        pack.validate
        expect(pack).not_to be_valid
      end
    end

    context 'when the pack is valid' do
      let(:pack) { described_class.new file: file_fixture('valid.yml') }

      it 'triggers validation on collections' do
        pack.validate
        expect(pack).to be_valid
      end
    end

    context 'when the pack was not validated' do
      let(:pack) { described_class.new file: file_fixture('invalid.yml') }

      it 'is considered valid' do
        expect(pack).to be_valid
      end
    end
  end

  describe '.normalize' do
    let(:pack) { described_class.new file: file_fixture('incomplete.yml') }

    before { pack.normalize }

    it 'removes empty, unused fields' do
      expect(pack.families.list[1].values).not_to have_key :source
    end

    it 'adds required fields with no value' do
      expect(pack.resources.list[0].values).to have_key :genus
    end
  end
end
