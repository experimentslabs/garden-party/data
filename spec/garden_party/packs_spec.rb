# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Pack files' do # rubocop:disable RSpec/DescribeClass
  Dir.glob(File.join('data', '*.yml')).each do |file|
    it "#{file} is a valid pack file" do
      pack = GardenParty::TrustedData::Pack.new file: file
      pack.validate
      expect(pack.valid?).to be true
    end
  end
end
