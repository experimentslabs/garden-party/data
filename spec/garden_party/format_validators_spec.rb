# frozen_string_literal: true

require 'spec_helper'

# rubocop:disable RSpec/MultipleExpectations
module GardenParty
  module TrustedData
    RSpec.describe FormatValidator do
      describe '.validate!' do
        it 'validates arrays' do
          expect { described_class.validate! [], :array }.not_to raise_error
          expect { described_class.validate! [1, 'a'], :array }.not_to raise_error
          expect { described_class.validate! 1, :array }.to raise_error ValidationError
        end

        it 'validates booleans' do
          expect { described_class.validate! true, :boolean }.not_to raise_error
          expect { described_class.validate! false, :boolean }.not_to raise_error
          expect { described_class.validate! 1, :boolean }.to raise_error ValidationError
        end

        it 'validates hashes' do
          expect { described_class.validate!({ 'abc' => 123 }, :hash) }.not_to raise_error
          expect { described_class.validate! 1, :hash }.to raise_error ValidationError
        end

        it 'validates numbers' do
          expect { described_class.validate! 1, :number }.not_to raise_error
          expect { described_class.validate! 1.1, :number }.not_to raise_error
          expect { described_class.validate! '1', :number }.to raise_error ValidationError
        end

        it 'validates strings' do
          expect { described_class.validate! 'abc', :string }.not_to raise_error
          expect { described_class.validate! 1, :string }.to raise_error ValidationError
        end

        # Arrays

        it 'validates lists of integers' do
          expect { described_class.validate! [1, 2], :integers_list }.not_to raise_error
          expect { described_class.validate! [1, '2'], :integers_list }.to raise_error ValidationError
        end

        it 'validates lists of strings' do
          expect { described_class.validate! %w[1 a], :strings_list }.not_to raise_error
          expect { described_class.validate! [1, 'a'], :strings_list }.to raise_error ValidationError
        end

        it 'validates lists of unique values' do
          expect { described_class.validate! [1, 'a', true], :uniques_list }.not_to raise_error
          expect { described_class.validate! [11, 'a', true, true], :uniques_list }.to raise_error ValidationError
        end

        # Hashes

        it 'validates hashes of symbol/string hashes' do
          expect { described_class.validate!({ abc: '123' }, :symbol_string_hash) }.not_to raise_error
          expect { described_class.validate!({ 'hello' => '123' }, :symbol_string_hash) }.to raise_error ValidationError
          expect { described_class.validate!({ hello: 123 }, :symbol_string_hash) }.to raise_error ValidationError
        end

        # Special formats

        it 'validates lists of months' do
          expect { described_class.validate! [1, 2], :months_list }.not_to raise_error
          # Min/max
          expect { described_class.validate! [-1, 2], :months_list }.to raise_error ValidationError
          expect { described_class.validate! [1, 22], :months_list }.to raise_error ValidationError
          # Integers
          expect { described_class.validate! [1.0, 2], :months_list }.to raise_error ValidationError
          # Unique
          expect { described_class.validate! [1, 2, 2], :months_list }.to raise_error ValidationError
        end

        it 'validates list of tags' do
          expect { described_class.validate! ['a', 'b', 'c::d'], :tags_list }.not_to raise_error
          # Uniques
          expect { described_class.validate! ['a', 'b', 'c::d', 'a'], :tags_list }.to raise_error ValidationError
          # Strings
          expect { described_class.validate! [1, 'b', 'c::d'], :tags_list }.to raise_error ValidationError
          # Tags separators
          expect { described_class.validate! ['c:::d'], :tags_list }.to raise_error ValidationError
          expect { described_class.validate! ['c:d'], :tags_list }.to raise_error ValidationError
        end

        it 'validates values in allow list' do
          expect { described_class.validate! 'hello', %w[hello world] }.not_to raise_error
          expect { described_class.validate! 'not valid', %w[hello world] }.to raise_error ValidationError
        end
      end
    end
  end
end
# rubocop:enable RSpec/MultipleExpectations
