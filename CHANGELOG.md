## [Unreleased]

## [0.0.5] - 2022-04-21

- Pack:
  - Add support for resource interactions
- Resource:
  - Sort some fields

## [0.0.4] - 2022-02-11

- Pack:
  - Add `synchronize` method to sync with another pack
  - `display_errors` now return nil
- Resources collection:
  - Enforce parents not to have parents
- Collection:
  - Sort entries by ID during normalization
- Rake tasks:
  - New task to find resources without parent

## [0.0.3] - 2022-01-10

- Pack:
  - `new` now handles a pack name
  - `path` can be changed on pack instances
  - Add `clone` method to duplicate a pack
- Collection:
  - Add `find` to find a record
  - Add `find_all` to find a list of records
  - Add `find_all_partial` to find a list of records with partial matches
  - Add `update_all` to update a list of records
- Record:
  - `get` now returns the field's default value when `nil`
- Console:
  - Add helpers to manipulate packs
  - Add welcome message with help

## [0.0.2] - 2021-12-25

- Start of data localization with french pack being renamed to `fr_72` 
- Adds data validation:
  - `:array` (generic array)
  - `:hash` (generic hash)
  - `:boolean`
  - `:integer`
  - `:number` (integer or float)
  - `:string`
  - `:integers_list` (array of integers)
  - `:strings_list` (array of strings)
  - `:uniques_list` (array of unique values)
  - `:months_list` (list of unique numbers from 1 to 12 included)
  - `:tags_list` (list of well formatted tags)
  - `:symbol_string_hash` (hash with symbol keys and string values)
- Pack validation in CI
- Adds `external_ids` key in resources to store unique identifiers of the resource in foreign systems/databases. Uniqueness of these identifiers is checked.
- Make names unique, as they are used as references for now (families, genera, resources)
- Drop `edible` attribute and add tag `edible` in resources
- minor other changes

## [0.0.1] - 2021-12-17

- Initial release
