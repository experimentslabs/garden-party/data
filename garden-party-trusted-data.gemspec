# frozen_string_literal: true

require_relative 'lib/garden_party/trusted_data/version'

Gem::Specification.new do |spec|
  spec.name          = 'garden_party-trusted_data'
  spec.version       = GardenParty::TrustedData::VERSION
  spec.authors       = ['Manuel Tancoigne']
  spec.email         = ['manu@experimentslabs.com']

  spec.summary       = 'Utility gem for GardenParty data reference'
  spec.description   = <<~TXT
    Methods and tasks to validate and manage GardenParty data.

    Garden Party is an opensource application to help beginners manage their garden.

    Use the gem repository to contribute to a source of truth for Garden Party!
  TXT
  spec.homepage = 'https://garden-party.io'
  spec.license  = 'MIT'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.0')

  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/experimentslabs/garden-party/data'
  spec.metadata['bug_tracker_uri'] = 'https://gitlab.com/experimentslabs/garden-party/data/issues'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/experimentslabs/garden-party/data/-/blob/main/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'activesupport'

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.metadata = {
    'rubygems_mfa_required' => 'true',
  }
end
