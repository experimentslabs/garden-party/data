# frozen_string_literal: true

require 'active_support'

require_relative '../lib/garden_party/trusted_data'
COLLECTIONS = [:families, :genera, :resources].freeze

namespace :data do
  desc 'Checks for errors in seed files.'
  task :check do
    errors = false
    Dir.glob(File.join(GardenParty::TrustedData::DATA_PATH, '*.yml')).each do |file|
      puts "\n#{file}\n#{'-' * file.length}"
      pack = GardenParty::TrustedData::Pack.new file: file
      pack.validate

      pack.display_errors

      if pack.valid?
        puts ActiveSupport::LogSubscriber.new.send(:color, 'is valid', :green)
      else
        errors = true
      end
    end

    abort('The data files needs attention') if errors
  end

  desc 'Add missing ids'
  task :add_ids do
    Dir.glob(File.join(GardenParty::TrustedData::DATA_PATH, '*.yml')).each do |file|
      pack = GardenParty::TrustedData::Pack.new file: file
      pack.add_ids
      pack.save
    end
  end

  desc 'Normalizes data by removing empty data'
  task :normalize do
    Dir.glob(File.join(GardenParty::TrustedData::DATA_PATH, '*.yml')).each do |file|
      pack = GardenParty::TrustedData::Pack.new file: file
      pack.normalize
      pack.save
    end
  end

  desc 'Exports packs in JSON format'
  task :export_json, [:output_directory, :packs] do |_t, args|
    output_directory = args.output_directory || Dir.pwd
    packs = args.packs ? args.packs.split(';').filter_map(&:chomp) : []

    raise 'Output directory does not exist' unless File.directory? output_directory

    packs = if packs.empty?
              Dir.glob File.join(GardenParty::TrustedData::DATA_PATH, '*.yml')
            else
              packs.map { |p| File.join(GardenParty::TrustedData::DATA_PATH, "#{p}.yml") }
            end
    packs.each do |pack_name|
      raise "No data file for pack '#{pack_name}'" unless File.exist? pack_name

      pack = GardenParty::TrustedData::Pack.new file: pack_name
      output_file = "#{File.basename(pack_name, '.yml')}.json"
      File.write File.join(output_directory, output_file), JSON.pretty_generate(pack.to_h)
    end
  end

  desc 'Display metrics in a Gitlab-CI format'
  task :metrics do
    output = ''

    Dir.glob(File.join(GardenParty::TrustedData::DATA_PATH, '*.yml')).each do |pack|
      pack_name = File.basename(pack, '.yml')
      pack      = GardenParty::TrustedData::Pack.new file: pack
      pack.collection_names.each do |collection|
        output += "#{pack_name}_#{collection}_count #{pack.send(collection).count}\n"
      end
    end

    puts output
  end
end
