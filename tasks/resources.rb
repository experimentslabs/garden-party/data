# frozen_string_literal: true

require 'active_support'

require_relative '../lib/garden_party/trusted_data'

namespace :resources do
  desc 'List resources without parent'
  task :parentless, [:pack] do |_t, args|
    pack_name = args.pack
    raise "No pack specified. usage: rake 'resource:parentless[<pack>]'" unless pack_name

    pack = GardenParty::TrustedData::Pack.new pack_name
    list = pack.resources.list
               .select { |r| r.values[:parent].nil? }
               .map { |r| r.values[:name] }.sort

    puts list.to_yaml
  end
end
